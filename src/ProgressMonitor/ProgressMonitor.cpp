#include "ProgressMonitor.h"

//ProgressMonitor::ProgressMonitor() : nmax(100), rate(1){
//}

ProgressMonitor::ProgressMonitor(unsigned long long _n, unsigned long long _r) : nmax(_n), rate(_r) {
    //nmax = _n;
    //rate = _r;
    start = std::chrono::steady_clock::now();
    stop = std::chrono::steady_clock::now();
    position = 0;
    if(rate==0){
        std::cout << "Cannot initialize ProgressMonitor with output rate 0. Setting output rate to 1." << std::endl;
    }
}

/*ProgressMonitor::ProgressMonitor() : ProgressMonitor(nullptr, nullptr){
    //start = std::chrono::steady_clock::now();
    //stop = std::chrono::steady_clock::now();
    //position = 0;
    //if(rate==0){
    //    std::cout << "Cannot initialize ProgressMonitor with output rate 0. Setting output rate to 1." << std::endl;
    //}
}
*/

unsigned long long ProgressMonitor::Update(){
    return ++position;    
}
void ProgressMonitor::Update(uint64_t _pos){
    //printf("%lu %lu\n", _pos, _pos);
    position = _pos;
}
//void ProgressMonitor::SetRate(unsigned long long _r){
//    rate = _r;
//    if(rate==0){
//        std::cout << "Cannot set ProgressMonitor output rate to 0. Setting output rate to 1." << std::endl;
//    }
//}
void ProgressMonitor::SetPosition(unsigned long long _i){
    position = _i;
}
//void ProgressMonitor::SetMax(unsigned long long _n){
    //nmax = _n;
//}

unsigned long long ProgressMonitor::GetPosition(){ return position;}
unsigned long long ProgressMonitor::GetMax(){ return nmax;}
unsigned long long ProgressMonitor::GetRate(){ return rate;}

void ProgressMonitor::Output(){

    if( (position%rate)!=0 ) return;
    
    double percent = ((double) position)/nmax*100;
    std::cout << "Progress: ";
    printf("%.2lf", (double) (position /  (double)(0x40000000) ) );
    std::cout << " GB / ";
    printf("%.2lf", (double) (nmax / (double)(0x40000000) ) );
    //printf("%.2lf", (double) (nmax / 1000000000.) );
    std::cout << " GB (";
    std::cout << std::fixed;
    std::cout.precision(2);
    std::cout << percent;
    std::cout << "%)";
    std::cout << std::endl;
    
}

void ProgressMonitor::Output(uint64_t _pos){

    position= _pos;
    
    double percent = ((double) position)/nmax*100;
    std::cout << "Progress: ";
    printf("%.2lf", (double) (position /  (double)(0x40000000) ) );
    std::cout << " GB / ";
    printf("%.2lf", (double) (nmax / (double)(0x40000000) ) );
    //printf("%.2lf", (double) (nmax / 1000000000.) );
    std::cout << " GB (";
    std::cout << std::fixed;
    std::cout.precision(2);
    std::cout << percent;
    std::cout << "%)";
    std::cout << std::endl;
    
}

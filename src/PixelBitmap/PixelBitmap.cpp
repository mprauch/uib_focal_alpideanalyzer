#include "PixelBitmap.h"
#include "../Geometry/Pixel.h"
#include <string>
#include <math.h>

PixelBitmap::PixelBitmap(int ru, int id){
    chipid = id;
    pbitmap = (bitmap*)calloc(1,sizeof(bitmap));
    pixelbuffer = (uint8_t*)malloc(_pixelbytesize);
    bitmapname = "pixelMask_1_" + std::to_string(chipid) + ".bmp";
}

int PixelBitmap::write(bool dummy){
    fp = fopen(bitmapname.c_str(),"wb");
    generateFileHeader();
    generateInfoHeader();
    setColorTable();
    fwrite (pbitmap, 1, sizeof(bitmap),fp);
    
    if(!dummy) writeDummyPixelMap();
    else  writePixelMap();

    fwrite(maskedpixels,1,_pixelbytesize,fp);
    fclose(fp);
    free(pbitmap);
    free(pixelbuffer);
    printf(" done!\n");
    return 1;
}

int PixelBitmap::setPixels(std::vector<Pixel> _px){
    px = _px;
    return true;    
}

void PixelBitmap::setChipId(int _id){
    chipid = _id;
}

int PixelBitmap::getChipId(){
    return chipid;
}

int PixelBitmap::writePixelMap(){
    for(unsigned int i =0;i<_pixelbytesize;i++){
        maskedpixels[i] = 0;
    }
    //
    for(unsigned int i=0;i<px.size();i++){
  
        unsigned int byte = ( px.at(i).y*_width + px.at(i).x) / 8 ;
        unsigned int bit = px.at(i).x % 8 ;
        uint8_t mask = (1 << (8-bit-1)  );
        maskedpixels[byte] = ( maskedpixels[byte] | mask );

    }
    return 1;
}

int PixelBitmap::writeDummyPixelMap(){
    for(unsigned int i =0;i<_pixelbytesize;i++){
        maskedpixels[i] = 0;
    }

    for(int x = 0; x< _width; x++){
        for(int y = 0; y< _height; y++){
            //if(x==2*y){
	    //if(x>=_width/2){
	    //if(y>=_height/2){
	    //if(y<=_height/8){
            //if(sqrt((x-256)*(x-256)+(y-256)*(y-256))<50){
            //if(x<10 && y <10){
            //if(x%4 && y%2){
	    int xcenter = (128*chipid+64);
	    //if( abs(x-xcenter)< 64){
	    if( (x>512) && (x-512 == y) && (x < 512+(chipid+1)*64)){
                Pixel p;
                p.x = x;
                p.y = y;
                px.push_back(p);
            }

        }
    }
    //
    for(unsigned int i=0;i<px.size();i++){
  
        unsigned int byte = ( px.at(i).y*_width + px.at(i).x) / 8 ;
        unsigned int bit = px.at(i).x % 8 ;
        uint8_t mask = (1 << (8-bit-1)  );
        maskedpixels[byte] = ( maskedpixels[byte] | mask );

    }
    return 1;
}


int PixelBitmap::generateInfoHeader(){
    pbitmap->infoheader.dibheadersize =sizeof(bitmapinfoheader);
    pbitmap->infoheader.width = _width;
    pbitmap->infoheader.height = _height;
    pbitmap->infoheader.planes = _planes;
    pbitmap->infoheader.bitsperpixel = _bitsperpixel;
    pbitmap->infoheader.compression = _compression;
    pbitmap->infoheader.imagesize = _pixelbytesize;
    pbitmap->infoheader.ypixelpermeter = _ypixelpermeter ;
    pbitmap->infoheader.xpixelpermeter = _xpixelpermeter ;
    pbitmap->infoheader.numcolorspallette = _numcolorspallette;
    return 1;
}

int PixelBitmap::generateFileHeader(){
    memcpy(pbitmap->fileheader.signature,"BM", sizeof("BM"));
    pbitmap->fileheader.filesize = _filesize;
    pbitmap->fileheader.fileoffset_to_pixelarray = sizeof(bitmap);
    return 1;
}

int PixelBitmap::setColorTable(){
    pbitmap->colortable[0].red = 0x00;
    pbitmap->colortable[0].green = 0x00;
    pbitmap->colortable[0].blue = 0x00;
    pbitmap->colortable[0].other = 0x00;
    pbitmap->colortable[1].red = 0xff;
    pbitmap->colortable[1].green = 0xff;
    pbitmap->colortable[1].blue = 0xff;
    pbitmap->colortable[1].other = 0x00;
    return 1;
}

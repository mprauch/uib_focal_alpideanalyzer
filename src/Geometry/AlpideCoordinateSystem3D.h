#ifndef ALPIDECOORDINATESYSTEM3D
#define ALPIDECOORDINATESYSTEM3D

#include "AlpideCoordinateSystem2D.h"

class AlpideCoordinateSystem3D : public AlpideCoordinateSystem2D {

    unsigned int layer;

    public:
        AlpideCoordinateSystem3D();
        AlpideCoordinateSystem3D(unsigned int _l, int _offx, int _offy, bool _invx, bool _invy);
        unsigned int GetLayer();
};
#endif //ALPIDECOORDINATESYSTEM3D

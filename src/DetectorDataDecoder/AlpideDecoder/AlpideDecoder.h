#pragma once

#ifndef ALPIDEDECODER_H
#define ALPIDEDECODER_H

#include <vector>
#include <functional>
#include <cstdint>

#include "TTree.h"
#include "TH2D.h"

#include "LaneData.h"
#include "AlpideLayerGeometry.h"

#define PIXELHITMAP

#ifndef ALPIDE_N_ROWS
#define ALPIDE_N_ROWS 0x200
#endif
#ifndef ALPIDE_N_COLS
#define ALPIDE_N_COLS 0x400
#endif

#define ALPIDE_MAXIMUM_HITS 8000

//#define ALPIDE_DEBUG

enum AlpideDataType {

// native alpide words    
    ALPIDE_IDLE, 
    ALPIDE_CHIPHEADER, 
    ALPIDE_CHIPTRAILER, 
    ALPIDE_EMPTYFRAME, 
    ALPIDE_REGIONHEADER, 
    ALPIDE_DATASHORT, 
    ALPIDE_DATALONG, 
    ALPIDE_BUSYON, 
    ALPIDE_BUSYOFF, 
    ALPIDE_LANE_ZERO,
    ALPIDE_UNKNOWN,

// alpide protocol extension words
    ALPIDE_EXTENSION_LANE_PADDING,
    ALPIDE_EXTENSION_LANE_ENTERING_STRIP_DATA_MODE,
    ALPIDE_EXTENSION_LANE_EXITING_STRIP_DATA_MODE,
    ALPIDE_EXTENSION_DETECTOR_TIMEOUT,
    ALPIDE_EXTENSION_OOT_ERROR_CRITICAL,
    ALPIDE_EXTENSION_PROTOCOL_ERROR,
    ALPIDE_EXTENSION_LANE_FIFO_OVERFLOW_ERROR,
    ALPIDE_EXTENSION_FSM_ERROR,
    ALPIDE_EXTENSION_PENDING_DETECTOREVENTS_LIMITS,
    ALPIDE_EXTENSION_PENDING_LANEEVENT_LIMITS,
    ALPIDE_EXTENSION_LANE_PROTOCOL_ERROR,
    ALPIDE_EXTENSION_OOT_ERROR_NON_CRITICAL
};


struct AlpidePixelData {
    TTree *datatree;
    TH2D *hitmap;
};

//typedef struct {
//  int chipId;
//  int region;
//  int dcol;
//  int address;
//} TPixHit;

class AlpideDecoder {
 private:
   uint8_t chipid;
   uint8_t headerflag;
   uint8_t trailerflag;
   unsigned int feeid;
   uint64_t orbit;
   ULong64_t trigger;
   unsigned char fatal;

   unsigned int byte;
   unsigned int byteMax;
   AlpideDataType type;
   LaneData data;

   bool mFatal; // lane FATAL

   bool mUseGlobalGeometry = false;

   static const long int maxtreesize = 0x40000000;
   //static const long int maxtreesize = 0x10000;
   
   //static void      DecodeChipHeader    (unsigned char *data, int &chipId, unsigned int &bunchCounter);
   //static void      DecodeChipTrailer   (unsigned char *data, int &flags);
   //static void      DecodeRegionHeader  (unsigned char *data, int &region);
   //static void      DecodeEmptyFrame    (unsigned char *data, int &chipId, unsigned int &bunchCounter);
   //static void      DecodeDataWord      (unsigned char *data, int chip, int region, std::vector<std::pair<int,int>> &hits, bool datalong, int &prioErrors);
   //static int       AddressToColumn     (int ARegion, int ADoubleCol, int AAddress);
   //static int       AddressToRow        (int ARegion, int ADoubleCol, int AAddress);
   static uint16_t AlpideX(uint8_t region, uint8_t encoder, uint16_t address);
   static uint16_t AlpideY(uint16_t address);
   std::vector<std::pair<uint16_t, uint16_t>> pixelhits;
   std::vector<std::pair<uint16_t, uint16_t>> globalpixelhits;
   std::pair<uint16_t, uint16_t> lpx;
   std::pair<uint16_t, uint16_t> gpx;


   TDirectory *objectdir;
   TFile *treefile;
   TFile *histfile;

   AlpideLayerGeometry *geometry = nullptr;
   AlpideCoordinateSystem3D *coordinates;

   bool NextByte();
   bool IsFatal(unsigned char word);
   bool RestOfLaneIsPadding();
   void PrintFatalMessage(AlpideDataType t);

   void CreateROOTObjects();
   void Fill();

 protected:
 public:
   AlpideDecoder();
   void initFiles(TFile *_treefile, TFile *_histfile);
   static AlpideDataType GetDataType(unsigned char dataWord);
   static int GetWordLength(AlpideDataType dataType);
   //bool DecodeEvent(unsigned char *data, int nBytes, std::vector<std::pair<uint16_t,uint16_t>> &hits, unsigned int &bunch_cnt, int &prioErrors);
   //bool DecodeEvent(unsigned char *data, int nBytes, unsigned int &bunch_cnt, int &prioErrors);
   bool DecodeEvent(LaneData &lanedata, TriggerData triggerdata /*, unsigned int &bunch_cnt, int &prioErrors*/);



   static bool ExtractNextEvent(unsigned char *data, int nBytes, int &eventStart, int &eventEnd, bool& isError, bool logging=false);
   static int NUMBER_OF_BUSY_VIOLATIONS;
   static int NUMBER_OF_BUSIES;

   void SetFeeId(unsigned int _f);
   void SetOrbit(uint64_t _orbit);
   void SetTrigger(uint64_t _t);

    TTree *pixelhittree;
    TTree *fataltree;

#ifdef PIXELHITMAP
    TH2D* pixelhitmap;
    TH1D* numberofhits;
    TH2D* globalpixelhitmap;
#endif
    void WriteToFile(TFile *rootfile);
    void WriteToFile();
    void SetTreeFile(TFile *rootfile);
    void SetHistFile(TFile *histfile);

    void SetAlpideLayerGeometry(AlpideLayerGeometry *geometry);


};

#endif

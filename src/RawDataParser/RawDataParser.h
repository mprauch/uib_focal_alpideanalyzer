#ifndef RAWDATAPARSER
#define RAWDATAPARSER

#include <iostream>
#include <fstream>
#include "RawDataHeader.h"
#include "ProgressMonitor.h"
#include "LaneData.h"

#include "TTree.h"

#define PM_OUTPUT_RATE 0x2000000
//#define DATA_OUTPUT

struct ParserConfig {

    // print options
    bool printRDH;          // printRDH
    bool printRDHDetails;   // printRDHDetails
    bool printDetectorData; // printDetectorData
    bool printLaneSummary;  // printLaneSummary

    //data format
    bool ddw;
    bool noDetectorData;

    //nData
    int nRDH;

    bool useGlobalGeometry=false;

    //output directory
    std::string outputDirectory="./";
};

enum ParserMode {
    NEW_PACKET,
    NEW_PAGE,
    DETECTOR_DATA,
    FINISHED
};

class RawDataParser{

    public:

        RawDataParser(std::string _fp, std::string _cp);
	void init(TFile *_writefile);
        int start();
        //std::streampos step(int stepsize);
        bool readRDH();
        bool readDetectorData();
        void resetLanes();
        void printLaneSummary();
        bool good();

        bool onlyRDH;
        RawDataHeader rdh;
        ParserMode mode;
        ParserConfig config;
        unsigned int packetsize;
        long long nextpacket;

        //unsigned char detectordata[LENGTH_DATA];
        //unsigned char identifier;
        //unsigned char padding[LENGTH_PADDING];

	char *detectordata;
	char *identifier;
	char *padding;              

        int loadConfig(std::string configfilepath);

        uint64_t trigger;
        uint64_t orbit;
        uint16_t bx;
	uint16_t memsize;
	uint16_t page;

        LaneData allLanes[N_LANES];
        TriggerData allTriggers[N_LANES];
        unsigned int feeid;

        ProgressMonitor *progressmonitor;
	TTree *rdhtree;
	TFile *writefile;

	void SetWriteFile(TFile *f);
	void WriteToFile();
	void WriteToFile(TFile *f);

	bool exit=false;

    private:
        std::ifstream infile;

        std::string filename;
        std::string filepath;
        unsigned long long position;

     	static const long int maxtreesize = 0x40000000;
    


};

#endif

# Continuous Reader
This software is adapted from the original ROOT implementation of a RAW file parser for ITS data.
It will continuously read a given file and write the pixel data to shared memory opened by other software,
e.g. the renderer in ../renderer. The reader will start at the end of the existing file and continuously
try to read more data as it is being written.

To use the reader, the renderer must first be started with a shared memory name and the reader has to be
configured through args such that the different readout units appearing in the RAW file will be mapped
to one of the shared memory spaces defined by the renderer instance(s).

## Usage
`continuous_reader [-v] [-o eof_offset] [--ru ru_id shared_memory_name flip_y ...] -f input_file`

- -v: Verbose. Adds some debug output, such as RDH header information and parsed pixels.
- --ru: Defines a readout unit. Followed by 3 additional parameters specifying the ID of the RU, the name of the
  shared memory to map the RU to, and if the chips from the RU should be flipped in the y-coordinate.
- -f: Input file to read from.
- -o: EOF offset. Starts reading x bytes before the end of the file, default: 0.

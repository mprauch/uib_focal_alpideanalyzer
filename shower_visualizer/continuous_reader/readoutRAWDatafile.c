#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#define resx 3072
#define resy 3072
#define SIZE (resx*resy*sizeof(uint32_t))
//#define DEBUG

int calculateALPIDEAdress(int addr);
int getAlpideX(uint8_t region, uint8_t encoder, uint16_t address);
int getAlpideY(uint16_t address);

void drawHit(unsigned int *ru, int x, int y, uint32_t color);

int chip_to_world_x(int chipid, int x);
int chip_to_world_y(int ru, int chipid, int y);

long int getFilesize(FILE *file);
FILE* openFile(char *filename);

struct RDHPacket {
    uint8_t headerVersion;
    uint8_t headerSize;
    uint16_t feeID;
    uint8_t priorityBit;
    uint8_t sourceID;
    uint16_t reserved;
    uint16_t offsetNewPacket;

    uint16_t bc : 12;
    uint32_t reserved2 : 20;
    uint32_t orbit;
    uint16_t reserved3;

    uint32_t trgType;
    uint16_t pagesCounter;
    uint8_t stopBit;
    uint32_t reserved4 : 24;
    //more columns can be found inside the manuals, but not yet added here.
};

struct ReadoutUnit {
    int flipped;
    uint32_t *location;

} ru_infos[16];

struct AlpideDataOptions {
    unsigned int offs;
    unsigned int chipID;
    unsigned int bunchCounter;
    unsigned int regionID;
    unsigned int feeIDGBTLink;
    uint8_t bufferUsed;
    unsigned char buffer[2];
} alpideDataOpts[128];

unsigned char getALPIDEByte(unsigned char *buffer, int position, struct AlpideDataOptions *alpideOpts);
void analyseAlpideData(unsigned char *alpideData, int length, struct AlpideDataOptions *alpideOpts);


int main(int argc, char *argv[]) {
    char filename[128] = "";
    long eofOffset = -1;
    long maxDistanceEOF = -1;

    // Argument parsing
    char current_arg[256];
    for (int i = 1; i < argc; ++i) {
        strncpy(current_arg, argv[i], 255);
        if (strncmp(current_arg,"--ru",255) == 0) {
            if (argc - i < 4) {
                printf("Too few arguments for RU (--ru)");
                return 1;
            }

            int ru = atoi(argv[++i]);
            char shm_name[64];
            strncpy(shm_name, argv[++i], 63);

            int fd = shm_open(shm_name, O_CREAT | O_RDWR, 0600);
            if (fd < 0) {
                printf("shm_open()");
                return 1;
            }
            ftruncate(fd, SIZE);
            ru_infos[ru].location = (uint32_t *)mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

            ru_infos[ru].flipped = (strncmp(argv[++i], "1", 10) == 0|| strncmp(argv[i], "true", 10) == 0);

        }
        else if (strncmp(current_arg,"-f",255) == 0) {
            if (argc - i < 2) {
                printf("Too few arguments for input file (-f)");
                return 1;
            }
            strncpy(filename, argv[++i], 127);
        }
        else if (strncmp(current_arg,"-o",255) == 0) {
            if (argc - i < 2) {
                printf("Too few arguments for eof offset (-o)");
                return 1;
            }
            eofOffset = atol(argv[++i]);
        }
        else if (strncmp(current_arg,"--mo",255) == 0) {
            if (argc - i < 2) {
                printf("Too few arguments for max distance to fileend (--mo)");
                return 1;
            }
            maxDistanceEOF = atol(argv[++i]);
        }
        else if (strncmp(current_arg,"-h",255) == 0 || strncmp(current_arg,"--help",255) == 0 ) {
            printf("Usage: %s [-v] [-o eof_offset] [--ru ru_id shared_memory_name flip_y ...] -f input_file\n", argv[0]);
            return 0;
        }
    }

    if (argc <= 5){
        printf("Usage: %s [-v] [-o eof_offset] [--ru ru_id shared_memory_name flip_y ...] -f input_file\n", argv[0]);
        exit(EINVAL);
    }
    FILE *rawFile = openFile(filename);
    printf("Exporting data from raw datafile: %s\n", filename);
    printf("File has a size of %li bytes\n", getFilesize(rawFile));

    unsigned char *buffer = malloc(1600);

    if(eofOffset != -1){
        long fileOffset = getFilesize(rawFile)-eofOffset;

        fseek(rawFile, fileOffset - fileOffset%16, SEEK_SET);
    }

    while ( 1 ){
        long filesize = getFilesize(rawFile);
        long currentPos = ftell(rawFile)+16;
        // Check if we have the end of the file -> then wait and check it again.
        if(filesize < currentPos){
            printf("Reached end of the file, waiting for new data..\n");
            sleep(3);
            continue;
        }

        if(maxDistanceEOF != -1 && filesize < ftell(rawFile)+maxDistanceEOF){
            printf("The difference between the end of the file and the current position is to high skipping some bytes.\n");
            printf("Current position: %li, Fileend: %li\n", ftell(rawFile), filesize);
        }

        #ifdef DEBUG
        printf("+++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        #endif

        fread(buffer, 16, 1, rawFile);

        if(feof(rawFile)) break;


        //Currently we are looking for RDH Packets
        if(buffer[0] != 0x06){
            printf("Current packet seems not to be a RDH Packet(%x) -> Something is weird, skipping and searching for RDH header..\n", buffer[0]);
            printf("Current file location: %li\n", ftell(rawFile));
            //exit(EIO);
            continue;
        }

        struct RDHPacket *rdhPacket = (struct RDHPacket*) buffer;
        unsigned int feeIDGBTLink = rdhPacket->feeID & 0x0f;

        // read in the rest of the header packet // The first 16 bit we already read thats why we have to skip them.
        fread(buffer+16, rdhPacket->offsetNewPacket-16, 1, rawFile);
        //printf("Page Counter: %i\n", rdhPacket->pagesCounter);
        // ---------------------------------------------------------------------------------------------------------
        if (rdhPacket->headerSize == rdhPacket->offsetNewPacket)
            continue;
        unsigned char *idsPacketStartAddress = ((unsigned char*)rdhPacket)+rdhPacket->headerSize;
        //printf("IDS Packet ID: %x\n", idsPacketStartAddress[9]);

        // To analyse the IDS Packet it is important to know, that it can happen that after a ids data trailer another diagnostic packet arrived.

        // Loop through the packets ater the header
        int isAlpideData = 0;


        for(int offset = 0; offset < rdhPacket->offsetNewPacket - rdhPacket->headerSize; offset=offset+16){
           unsigned char *currentReadPosition = idsPacketStartAddress+offset;
            if(currentReadPosition[9] == 0xe8){
                //printf("Trigger Data Header\n");
                isAlpideData = 1;

            }else if(currentReadPosition[9] == 0xf0){
                //printf("Trigger Data Trailer\n");
                isAlpideData = 0;

            }else if(isAlpideData){
                //printf("Received ALPIDE Data\n");
                if(currentReadPosition[9] >= 128){
                    printf("Chip is outside of map. Please increase the mapsize.\n");
                    exit(1);
                }else{
                    struct AlpideDataOptions *opts = &alpideDataOpts[currentReadPosition[9]];
                    opts->feeIDGBTLink = feeIDGBTLink;
                    analyseAlpideData(currentReadPosition, 9, opts);
                }
                
            }else if(currentReadPosition[9] == 0xe0){
                //printf("ITS Header Word\n");
            }else if(currentReadPosition[9] == 0xe4){
                //printf("Diagnostig Data Word\n");
            }else if(currentReadPosition[9] == 0xf8){
                //printf("Calibration Data Word\n");
            }else{
                printf("The ITS packet type: %x is not yet implemented. Skipping..\n", currentReadPosition[9]);
                printf("Current file location: %li\n", ftell(rawFile));
                continue;
            }
        }
    }

    fclose(rawFile);
    return 0;
}



/**
 * Get the found ALPIDE data and parse it as described inside the alpide manual
 * @param alpideData pointer to the location where the alpide data can be found
 * @param length length of the alpide data in byte which has to be parsed.
 **/
void analyseAlpideData(unsigned char *alpideData, int length, struct AlpideDataOptions *alpideOpts){
    #ifdef DEBUG
    printf("--------------------------------------------------\n");

    printf("ALPIDE: %02x %02x %02x %02x %02x   %02x %02x %02x %02x %02x\n", alpideData[0], alpideData[1], alpideData[2], alpideData[3],
            alpideData[4], alpideData[5],alpideData[6], alpideData[7], alpideData[8], alpideData[9]);
    #endif
    // Check if chip is idling
    unsigned int offset = 0;


    length = length + alpideOpts->bufferUsed;

    #ifdef DEBUG
    printf("Current length: %i\n", length);
    #endif

    unsigned int encoderID = 0;
    unsigned int addr = 0;



    while( 1 ){
        #ifdef DEBUG
        printf("Current offset: %i\n", offset);
        #endif
        if(offset >= length){
            #ifdef DEBUG
            printf("Finished ALPIDE Data analysis.\n");            
            #endif
            alpideOpts->bufferUsed = 0;
            break;
        }


        if(getALPIDEByte(alpideData, offset+0, alpideOpts) == 0xff){
            #ifdef DEBUG
            printf("Chip is idling.");
            #endif
            offset+=1;

        // Check if is an empty frame
        }else if ((getALPIDEByte(alpideData, offset+0, alpideOpts) & 0xf0) == 0xe0){
            if(offset + 2 > length){
                #ifdef DEBUG
                printf("Waiting to parse splitted Header packet.\n");
                #endif
                alpideOpts->buffer[0] = getALPIDEByte(alpideData, offset+0, alpideOpts);
                alpideOpts->bufferUsed = 1;
                break;
            }
            //1110<chip id[3:0]><BUNCH COUNTER FOR FRAME[10:3] >
            alpideOpts->chipID = (getALPIDEByte(alpideData, offset+0, alpideOpts) & 0x0f);
            alpideOpts->bunchCounter = getALPIDEByte(alpideData, offset+1, alpideOpts);
            #ifdef DEBUG
            printf("Empty Frame || chipID:%i, bunchCounter:%i\n", alpideOpts->chipID, alpideOpts->bunchCounter);
            #endif
            offset+=2;
            break;
        // Check if is an chip header
        }else if((getALPIDEByte(alpideData, offset+0, alpideOpts) & 0xf0) == 0xa0){
            if(offset + 2 > length){
                #ifdef DEBUG
                printf("Waiting to parse splitted Chip packet.\n");
                #endif
                alpideOpts->buffer[0] = getALPIDEByte(alpideData, offset+0, alpideOpts);
                alpideOpts->bufferUsed = 1;
                break;
            }
            //1010<chip id[3:0]><BUNCH COUNTER FOR FRAME[10:3] >
            alpideOpts->chipID = (getALPIDEByte(alpideData, offset+0, alpideOpts) & 0x0f);
            alpideOpts->bunchCounter = getALPIDEByte(alpideData, offset+1, alpideOpts);
            #ifdef DEBUG
            printf("Received Chip Header || chipID:%i, bunchCounter:%i\n", alpideOpts->chipID, alpideOpts->bunchCounter);
            #endif
            offset+=2;

        // Check if is an chip trailer
        }else if((getALPIDEByte(alpideData, offset+0, alpideOpts) & 0xf0) == 0xb0){
            //1011<readout flags[3:0]>
            unsigned int readoutFlags = (getALPIDEByte(alpideData, offset+0, alpideOpts) & 0x0f);
            #ifdef DEBUG
            printf("Received Chip Trailer\n");
            #endif
            offset+=1;
            alpideOpts->bufferUsed = 0;
            break;

        // Check if is an Region Header
        }else if((getALPIDEByte(alpideData, offset+0, alpideOpts) & 0xe0) == 0xc0){
            alpideOpts->regionID = (getALPIDEByte(alpideData, offset+0, alpideOpts) & 0x1f);
            #ifdef DEBUG
            printf("Received Region Header\n");
            #endif
            offset+=1;
        // Check if is an Data Long
        }else if((getALPIDEByte(alpideData, offset+0, alpideOpts) & 0xc0) == 0x00 && (getALPIDEByte(alpideData, offset+2, alpideOpts) & 0x80) == 0x00){
            if(offset + 2 > length){
                #ifdef DEBUG
                printf("Waiting to parse splitted Data Long packet.(2)\n");
                #endif
                alpideOpts->buffer[0] = getALPIDEByte(alpideData, offset+0, alpideOpts);
                alpideOpts->bufferUsed = 1;
                break;
            }else if(offset + 3 > length){
                #ifdef DEBUG
                printf("Waiting to parse splitted Data Long packet.(3)\n");
                #endif
                alpideOpts->buffer[0] = getALPIDEByte(alpideData, offset+0, alpideOpts);
                alpideOpts->buffer[1] = getALPIDEByte(alpideData, offset+1, alpideOpts);
                alpideOpts->bufferUsed = 2;
                break;
            }

            encoderID = (getALPIDEByte(alpideData, offset+0, alpideOpts) & 0x3c) >> 2;
            addr = (getALPIDEByte(alpideData, offset+0, alpideOpts) & 0x03) << 8;
            addr = addr + getALPIDEByte(alpideData, offset+1, alpideOpts);
            int hitmap = (getALPIDEByte(alpideData, offset+2, alpideOpts) & 0x7f);	    

            int y = getAlpideY(addr);
            int x = getAlpideX(alpideOpts->regionID, encoderID, addr);
            int globalY = chip_to_world_y(alpideOpts->feeIDGBTLink, alpideOpts->chipID, y);
            int globalX = chip_to_world_x(alpideOpts->chipID, x);
            #ifdef DEBUG
            printf("Received Data Long chipID: %i, alpideX: %i, alpideY: %i, feeIDLink: %i, globalX: %i, globalY: %i, hitmap: %02x\n",
             alpideOpts->chipID, x, y, alpideOpts->feeIDGBTLink, globalX, globalY, getALPIDEByte(alpideData, offset+2, alpideOpts));
            #endif
            if(globalX > resx || globalY > resy){
                printf("Cant draw data long packet. Skipping..\n");
            }else{
                drawHit(&alpideOpts->feeIDGBTLink, globalX, globalY, 0xffffffff);
            }

	    for( int h=0; h<8; h++ ){
            addr++;
		    if(getALPIDEByte(alpideData, offset+2, alpideOpts) & (1 << h) ){
                int y = getAlpideY(addr);
                int x = getAlpideX(alpideOpts->regionID, encoderID, addr);
                int globalY = chip_to_world_y(alpideOpts->feeIDGBTLink, alpideOpts->chipID, y);
                int globalX = chip_to_world_x(alpideOpts->chipID, x);
                #ifdef DEBUG
                printf("Received Data Long hitmap chipID: %i, alpideX: %i, alpideY: %i, feeIDLink: %i, globalX: %i, globalY: %i, hitmap: %02x\n",
                    alpideOpts->chipID, x, y, alpideOpts->feeIDGBTLink, globalX, globalY, getALPIDEByte(alpideData, offset+2, alpideOpts));
                #endif
                if(globalX > resx || globalY > resy){
                    printf("Cant draw data long packet. Skipping..\n");
                }else{
                    drawHit(&alpideOpts->feeIDGBTLink, globalX, globalY, 0xffffffff);
                }
		 
		    }
	    } 
            offset+=3;
        // Check if is an Data Short
        }else if((getALPIDEByte(alpideData, offset+0, alpideOpts) & 0xc0) == 0x40){
            if(offset + 2 > length){
                #ifdef DEBUG
                printf("Waiting to parse splitted Data Short packet.\n");
                #endif
                alpideOpts->buffer[0] = getALPIDEByte(alpideData, offset+0, alpideOpts);
                alpideOpts->bufferUsed = 1;
                break;
            }
            encoderID = (getALPIDEByte(alpideData, offset+0, alpideOpts) & 0x3c) >> 2;
            addr = (getALPIDEByte(alpideData, offset+0, alpideOpts) & 0x03) << 8;
            addr = addr + getALPIDEByte(alpideData, offset+1, alpideOpts);

            int y = getAlpideY(addr);
            int x = getAlpideX(alpideOpts->regionID, encoderID, addr);
            int globalY = chip_to_world_y(alpideOpts->feeIDGBTLink, alpideOpts->chipID, y);
            int globalX = chip_to_world_x(alpideOpts->chipID, x);
            #ifdef DEBUG
            printf("Received Data Short chipID: %i, alpideX: %i, alpideY: %i, feeIDLink: %i, globalX: %i, globalY: %i\n",
             alpideOpts->chipID, x, y, alpideOpts->feeIDGBTLink, globalX, globalY);
            #endif
            if(globalX > resx || globalY > resy){
                printf("Cant draw data short packet. Skipping..\n");
            }else{
                drawHit(&alpideOpts->feeIDGBTLink, globalX, globalY, 0xffffffff);
            }
            offset+=2;

        // Check if is an Bussy On
        }else if(getALPIDEByte(alpideData, offset+0, alpideOpts) == 0xf1){
            #ifdef DEBUG
            printf("Received Bussy On\n");
            #endif
            offset+=1;

        // Check if is an Bussy Off
        }else if(getALPIDEByte(alpideData, offset+0, alpideOpts) == 0xf0){
            #ifdef DEBUG
            printf("Received Bussy Off\n");
            #endif
            offset+=1;

        }else{
            printf("Cant identify data type.. Skipping ALPIDE Data.\n");
            printf("ALPIDE: %02x %02x %02x %02x %02x   %02x %02x %02x %02x %02x\n", getALPIDEByte(alpideData, 0, alpideOpts), getALPIDEByte(alpideData, 1, alpideOpts),
                getALPIDEByte(alpideData, 2, alpideOpts),getALPIDEByte(alpideData, 3, alpideOpts),getALPIDEByte(alpideData, 4, alpideOpts),getALPIDEByte(alpideData, 5, alpideOpts),
                getALPIDEByte(alpideData, 6, alpideOpts),getALPIDEByte(alpideData, 7, alpideOpts),getALPIDEByte(alpideData, 8, alpideOpts),getALPIDEByte(alpideData, 9, alpideOpts));
            exit(1);
	        offset+=1;
            //break;
        }
    }

    #ifdef DEBUG
    printf("Offset final position: %i\n", offset);
    printf("Data-buffer final position: %i\n", alpideOpts->bufferUsed);
    #endif

}


unsigned char getALPIDEByte(unsigned char *buffer, int position, struct AlpideDataOptions *alpideOpts){
    if(alpideOpts->bufferUsed > 0 && position < alpideOpts->bufferUsed){
        return alpideOpts->buffer[position];
    }
    return buffer[position - alpideOpts->bufferUsed];
}

int calculateALPIDEAdress(int addr){
    return ((addr/9) * 7) + addr;
    //return ((addr/10) * 6) + addr;
}

int getAlpideY(uint16_t address){
    return address/2;
}

int getAlpideX(uint8_t region, uint8_t encoder, uint16_t address){
    int x = region*32 + encoder*2;
    if(address%4==1) x++;
    if(address%4==2) x++;
    return x;
}

int chip_to_world_x(int chipid, int x) {
    int world_x = x + (2 - (chipid % 3)) * 1024;
    return world_x;
}

int chip_to_world_y(int ru, int chipid, int y) {
    struct ReadoutUnit info = ru_infos[ru];
    if (info.flipped) {
        y = 511 - y;
    }
    else {
        y += 512;
    }
    int world_y = y + chipid/3 * 1024;
    return world_y;
}

void drawHit(unsigned int *ru, int x, int y, uint32_t color)
{
    if (*ru > 15) {
        printf("Found unconfigured readout unit.\n");
        return;
    }
    struct ReadoutUnit unit = ru_infos[*ru];
    if(unit.location == NULL){
        #ifdef DEBUG
        printf("ReadoutUnit seems to be null\n");
        #endif
        return;
    }
        if (x > 3071 || y > 3071) {
        #ifdef DEBUG
        printf("Pixel out of bounds: %d, %d", x, y);
        #endif
        return;
    }


    uint32_t *memory_thingy = unit.location;

    if (x > 0) memory_thingy[y * resx + x - 1] = color;
    if (x < 3071) memory_thingy[y * resx + x + 1] = color;
    if (y > 0) memory_thingy[(y - 1) * resx + x] = color;
    if (y < 3071) memory_thingy[(y + 1) * resx + x] = color;

    if (x > 0 && y > 0) memory_thingy[(y - 1) * resx + x - 1] = color;
    if (x < 3071 && y < 3071) memory_thingy[(y + 1) * resx + x + 1] = color;
    if (x > 0 && y < 3071) memory_thingy[(y + 1) * resx + x - 1] = color;
    if (x < 3071 && y > 0) memory_thingy[(y - 1) * resx + x + 1] = color;

    if (x > 1) memory_thingy[y * resx + x - 2] = color;
    if (x < 3070) memory_thingy[y * resx + x + 2] = color;
    if (y > 1) memory_thingy[(y - 2) * resx + x] = color;
    if (y < 3070) memory_thingy[(y + 2) * resx + x] = color;
}



/**
 * Get the size of a file
 * @param file File to get the size from
 * @return returns the size in bytes as long int
 **/
long getFilesize(FILE *file){
    long currentPosition = ftell(file);
    fseek(file, 0, SEEK_END);
    long filesize = ftell(file);
    fseek(file, currentPosition, SEEK_SET);
    return filesize;
}
/**
 * Search and open a file describer for a raw file  to analyse.
 * @param filename Filename
 * @return returns a file describer pointing to the file specified in the arguments.
 **/
FILE* openFile(char *filename){
    FILE *file = fopen(filename, "r");
    if (file == 0){
        printf("Raw file %s not found. Exiting.\n", filename);
        exit(ENOENT);
    }
    return file;
}

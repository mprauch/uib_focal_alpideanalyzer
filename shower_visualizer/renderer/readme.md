# Renderer

Simple pixel renderer to display quasi live data from alpide detectors. FOCAL Test Sep. 2021

## Building

To build the renderer use the following command

```
gcc -lrt -std=c11 -D_XOPEN_SOURCE=500 -o renderer -lSDL2 renderer.c
```

Make sure to install the SDL2 library.

## Usage

```
./renderer -m <shared memory path> [-d <decay delay in u-seconds >]
```

The renderer uses a shared memory section with the reader software to exchange pixel data. Both tools need to be configured with the same name for the shared memory section to work properly.

The reader writes the data of a single layer to a memory section, so to display multiple layers you need to open multiple instances of this tool, each pointing to a different memory section.

### Example calls

```
./renderer -m /layer1 -d 10000
```

```
./renderer -m /layer2 -d 10000
```
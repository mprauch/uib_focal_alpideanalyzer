#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>

#include <SDL2/SDL.h>
//#include <SDL2/SDL_ttf.h>

#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

#define resx 3072
#define resy 3072

#define win_resx 1024
#define win_resy 1024

#define SIZE (resx * resy * sizeof(Uint32))

typedef struct
{
    Uint32 *pixels;
    bool *quit;
} EventThreadData;

bool quit = false;
int decayDelay = 50000;

int randInt(int rmin, int rmax)
{
    return rand() % (rmax - rmin) + rmin;
}

static int decayThread(void *pixels)
{
    Uint32 *mypix = (Uint32 *)pixels;
    Uint32 *p;
    const Uint32 *end = &mypix[resx * resy];
    while (!quit)
    {
        for (p = mypix; p != end; ++p)
        {
            if (*p > 0x00000000)
                *p -= 0x0f0f0f0f;
        }
        usleep(decayDelay);
    }
    return 0;
}

void drawHit(Uint32 *pixels, int x, int y, Uint32 color)
{
    pixels[y * resx + x] = color;
    pixels[y * resx + x - 1] = color;
    pixels[y * resx + x] = color;
    pixels[y * resx + x + 1] = color;
    pixels[(y - 1) * resx + x] = color;
    pixels[(y + 1) * resx + x] = color;
    pixels[(y - 1) * resx + x - 1] = color;
    pixels[(y + 1) * resx + x + 1] = color;
    pixels[(y + 1) * resx + x - 1] = color;
    pixels[(y - 1) * resx + x + 1] = color;

    pixels[y * resx + x - 2] = color;
    pixels[y * resx + x] = color;
    pixels[y * resx + x + 2] = color;
    pixels[(y - 2) * resx + x] = color;
    pixels[(y + 2) * resx + x] = color;
    pixels[(y - 2) * resx + x - 2] = color;
    pixels[(y + 2) * resx + x + 2] = color;
    pixels[(y + 2) * resx + x - 2] = color;
    pixels[(y - 2) * resx + x + 2] = color;
}

int main(int argc, char **argv)
{
    char memname[50];
    bool gotmem=false;
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-m") == 0) {
            strncpy(memname,argv[++i],50);
            gotmem = true;
        }
        else if (strcmp(argv[i], "-d") == 0) {
            char *rest;
            decayDelay = strtol(argv[++i],&rest,10);
            if(*rest){
                printf("Cannot read decay delay: %s",rest);
                return EXIT_FAILURE;
            }
        }
        else if (strcmp(argv[i], "-h") == 0) {
            // -h shows usage info and returns
            printf("Usage: %s -m shared memory name [-d decay delay]", argv[0]);
            return 0;
        }
    }
    if(!gotmem){
        printf("Please provide a shared memory name!\n");
        return 0;
    }
    SDL_Thread *decay_Thread;
    SDL_Thread *event_Thread;
    SDL_Thread *randHit_Thread;

    int fd = shm_open(memname, O_CREAT | O_RDWR, 0600);
    if (fd < 0)
    {
        perror("shm_open()");
        return EXIT_FAILURE;
    }
    ftruncate(fd, SIZE);

    Uint32 *pixels = (Uint32 *)mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    memset(pixels, 0, resx * resy * sizeof(Uint32));

    decay_Thread = SDL_CreateThread(decayThread, "decayThread", pixels);

    SDL_Init(SDL_INIT_VIDEO);

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");

    SDL_Window *window = SDL_CreateWindow(memname,
                                          SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, win_resx, win_resy, 0);

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
    SDL_Texture *pixel_texture = SDL_CreateTexture(renderer,
                                                   SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, resx, resy);
    SDL_Event event;

    Uint32 oldtime, newtime;
    Uint32 times[60];
    int index = 0;
    float sum;

    char frametime[20];
    while (!quit)
    {
        SDL_UpdateTexture(pixel_texture, NULL, pixels, resx * sizeof(Uint32));
        
        while (SDL_PollEvent(&event) != 0)
        {
            switch (event.type)
            {
            case SDL_QUIT:
                quit = true;
                break;
            }
        }

        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, pixel_texture, NULL, NULL);
        SDL_RenderPresent(renderer);
    }

    SDL_WaitThread(decay_Thread, NULL);
    munmap(pixels, SIZE);
    close(fd);
    shm_unlink(memname);
    SDL_DestroyTexture(pixel_texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
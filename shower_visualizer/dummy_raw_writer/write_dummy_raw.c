#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

FILE *input_file = NULL;
FILE *output_file = NULL;
long time_delay = 0;
bool verbose = false;
bool repeat = false;

unsigned char buffer[16];

size_t read_buffer();
void write_buffer();

int main(int argc, char *argv[]) {
    // Argument parsing
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-i") == 0) {
            // -i is followed by the path to the input file
            input_file = fopen(argv[++i], "rb");
        }
        else if (strcmp(argv[i], "-o") == 0) {
            // -o is followed by the path to the output file
            output_file = fopen(argv[++i], "wb");
        }
        else if (strcmp(argv[i], "-t") == 0) {
            // -t is followed by the delay between 16 byte blocks in microseconds
            time_delay = atol(argv[++i]);
        }
        else if (strcmp(argv[i], "-v") == 0) {
            // -v is the flag for "verbose", which means the bytes are also written to stdout
            verbose = true;
        }
        else if (strcmp(argv[i], "-r") == 0) {
            // -r is the flag for "repeat", which means the input file will be repeated indefinitely
            repeat = true;
        }
        else if (strcmp(argv[i], "-h") == 0) {
            // -h shows usage info and returns
            printf("Usage: %s [-v] [-t time_delay] -i input_file -o output_file", argv[0]);
            return 0;
        }
    }

    if (input_file == NULL || output_file == NULL) {
        printf("Missing required arguments.\n");
        return 1;
    }

    // Continuously read from the input file and write to the output file
    while (read_buffer() == 16) {
        write_buffer();
        usleep(time_delay);
    }

    fclose(input_file);
    fclose(output_file);
}

size_t read_buffer() {
    size_t read_bytes = fread(buffer, 1, sizeof(buffer), input_file);
    if (read_bytes < 16 && repeat) {
        fseek(input_file, 0, SEEK_SET);
        read_bytes = fread(buffer, 1, sizeof(buffer), input_file);
    }
    return read_bytes;
}

void write_buffer() {
    if (verbose) {
        for (int i = 0; i < sizeof(buffer); ++i) {
            printf("%02x ", buffer[i]);
        }
        printf("\n");
    }

    fwrite(buffer, 1, sizeof(buffer), output_file);
}

# Dummy Raw Writer
This small piece of C-code is supposed to help debug live visualization tools by slowly
writing a .raw file in 16-byte blocks with delays in between. This is done by copying an
existing .raw file slowly into a different file.

## Usage
`write_dummy_raw [-vr] [-t time_delay] -i input_file -o output_file`

- -v: Verbose, i.e. the bytes are also written to stdout.
- -r: Repeat flag, indicating indefinite repeating of the input file in case the end is reached
- -t: The time delay between 16-byte blocks in microseconds.
- -i: Input file to copy.
- -o: output file to generate.

Specifying the input and output files is mandatory. The input file has to exist, the
output file will be created (or overridden, if it exists).

#include <iostream>
#define ALPIDE
#include "readGBTData.h"
//#include "readGBTData.C"
//#include "TTree.h"
//#include "TFile.h"

unsigned char alpide[3000] = {};
/*unsigned char alpide[] = {
    0xa4, 0x18, 0xc0, 0x01, 0xfe, 0x01, 0xc1, 0x01, 0xfe, 
    0x01, 0xc2, 0x01, 0xfe, 0x01, 0xc3, 0x01, 0xfe, 0x01, 
    0xc4, 0x01, 0xfe, 0x01, 0xc5, 0x01, 0xfe, 0x01, 0xc6, 
    0x01, 0xfe, 0x01, 0xc7, 0x01, 0xfe, 0x01, 0xc8, 0x01, 
    0xfe, 0x01, 0xc9, 0x01, 0xfe, 0x01, 0xca, 0x01, 0xfe, 
    0x01, 0xcb, 0x01, 0xfe, 0x01, 0xcc, 0x01, 0xfe, 0x01, 
    0xcd, 0x01, 0xfe, 0x01, 0xce, 0x01, 0xfe, 0x01, 0xcf, 
    0x01, 0xfe, 0x01, 0xd0, 0x01, 0xfe, 0x01, 0xd1, 0x01, 
    0xfe, 0x01, 0xd2, 0x01, 0xfe, 0x01, 0xd3, 0x01, 0xfe, 
    0x01, 0xd4, 0x01, 0xfe, 0x01, 0xd5, 0x01, 0xfe, 0x01, 
    0xd6, 0x01, 0xfe, 0x01, 0xd7, 0x01, 0xfe, 0x01, 0xd8, 
    0x01, 0xfe, 0x01, 0xd9, 0x01, 0xfe, 0x01, 0xda, 0x01, 
    0xfe, 0x01, 0xdb, 0x01, 0xfe, 0x01, 0xdc, 0x01, 0xfe, 
    0x01, 0xdd, 0x01, 0xfe, 0x01, 0xde, 0x01, 0xfe, 0x01, 
    0xdf, 0x01, 0xfe, 0x01, 0xb0
};
*/
int getAlpideY(uint16_t address);
int getAlpideX(uint8_t region, uint8_t encoder, uint16_t address);

unsigned int lengthAlpideData = 0;
void readALPIDEData(unsigned int length);
void readALPIDEData();

TH2D *newAlpideMap(int id){
    TH2D *h2 = new TH2D(Form("alpide%d", id), Form("alpide%d", id), 1024, 0, 1024, 512, 0, 512);
    return h2;
}


void readALPIDEData(unsigned int length){

    lengthAlpideData = length;
    readALPIDEData();
}

void readALPIDEData(){

    bool header = false;
    int dataword = 0;
    bool regionheader = false;

    uint8_t headermask = 0xf0;
    uint8_t trailermask = 0xf0;
    uint8_t chipidmask = 0x0f;
    uint8_t regionheadermask = 0xe0;
    uint8_t regionmask = 0x1f;
    uint8_t datamask = 0xc0;
    uint8_t encodermask = 0x3c; 
    uint8_t address1mask = 0x03;
    uint8_t address2mask = 0xff;

    uint8_t chipid = 0;
    uint8_t bxcounter = 0;
    uint8_t region = 0;;

    if(lengthAlpideData == 0){
        lengthAlpideData = sizeof(alpide);
    }

#ifdef PRINT_DEBUG
    //printf("(%d)\n", counter);
    for(unsigned int i=0;i<lengthAlpideData;i++){
        printf("%02x ", alpide[i]);
    }
    printf(" ##\n");
#endif

    //printf("%d\n", lengthAlpideData);
    for(unsigned int i=0;i<lengthAlpideData;i++){
        if(dataword>0) {
            dataword--;
        }
        //printf("%02x ", alpide[i]);
        if(!header && !dataword && ( (headermask & alpide[i])==0xa0 ) ){
            
            header = true;
            chipid = chipidmask & alpide[i];
            //if(alpideMaps.size() <= chipid){
            //    for(uint8_t m=alpideMaps.size();m<=chipid;m++){
            //        alpideMaps.push_back(newAlpideMap(m));
            //    }                
            //}
            //printf(" --> word %d: header found for chip id %d\n", i, chipid);

            i++;
            bxcounter = alpide[i];
            //printf("%02x", alpide[i]);
            //printf(" --> word %d: bxcounter is %d", i, bxcounter);

        }
        if(header && !dataword){
            if( (regionheadermask & alpide[i]) == 0xc0){
                regionheader = true;
                region = (regionmask & alpide[i]);
                //printf(" --> word %d: region header found for region %d", i, region);
            }
            if( (trailermask & alpide[i]) == 0xb0){
                //break;
                //printf(" --> word %02x: trailer found\n", alpide[i]);
                return;
            }
        }
        if(regionheader && header && !dataword){
            if( (datamask & alpide[i]) == 0x40){
                ////printf(" --> word %d: short data found", i);
                //printf("Filling short %02x %02x\n", alpide[i], alpide[i+1]);
                dataword = 2;
                uint8_t encoder  = (encodermask & alpide[i]) >> 2;
                uint16_t address = (address1mask & alpide[i]);
                address = address << 8;
                uint16_t a = (address2mask & alpide[++i]);
                //printf("%02x ", alpide[i]);
                ////printf("address: %d %02x %d", address, a, a+address);
                address+=a;
                //++i;
                ////printf("%02x ", alpide[++i]);
                int y = getAlpideY(address);
                int x = getAlpideX(region, encoder, address);
                //printf(" --> short data found with encoder %d, address %d, and hitmap 0x%02x", encoder, address, alpide[i]);
                //printf("\n--> position: (x = %d, y = %d)", x, y);
                dataword = 0;
#ifdef ALPIDE
                alpideX = x;
                alpideY = y;
                alpideID = chipid;
                alpideBX = bxcounter;
                //if(alpideX==1023) {
                //    for(unsigned int i=0;i<lengthAlpideData;i++){
                //        printf("%02x ", alpide[i]);
                //    }
                //    printf("## %02x %02x %02x ## ", region, encoder, address);
                //    printf("## %d %d %d ##\n", region, encoder, address);
                //}
                dataTree->Fill();
                //alpideMaps.at(chipid)->Fill(alpideX, alpideY);
#endif
            }
            else if( (datamask & alpide[i]) == 0x00){
                //printf("Filling long %02x\n", alpide[i]);
                uint8_t encoder  = (encodermask & alpide[i]) >> 2;
                //uint8_t encoder  = (encodermask & alpide[i]) ;
                uint16_t address = (address1mask & alpide[i]);
                address = address << 8;
                uint16_t a = (address2mask & alpide[++i]);
                //printf("%02x ", alpide[i]);
                ////printf("address: %d %02x %d", address, a, a+address);
                address+=a;
                i++;
                //printf("%02x ", alpide[i]);
                int y = getAlpideY(address);
                int x = getAlpideX(region, encoder, address);
                //printf(" --> %d long data found with encoder %d, address %d, and hitmap 0x%02x", lengthAlpideData, encoder, address, alpide[i]);
                //printf("\n--> position: (x = %d, y = %d)", x, y);
#ifdef ALPIDE
                alpideX = x;
                alpideY = y;
                alpideID = chipid;
                alpideBX = bxcounter;
                dataTree->Fill();
                //alpideMaps.at(chipid)->Fill(alpideX, alpideY);
                //if(alpideX == 1023) {
                //    for(unsigned int i=0;i<lengthAlpideData;i++){
                //        printf("%02x ", alpide[i]);
                //    }
                //    printf(" ##\n");
                //}
#endif

                for(int h=0;h<8;h++){
                    address++;
                    if( alpide[i] & (1 << h) ){
                        int y = getAlpideY(address);
                        int x = getAlpideX(region, encoder, address);
                        //printf("\n--> position: (x = %d, y = %d)", x, y);
#ifdef ALPIDE
                        //printf("Filling tree...\n");
                        alpideX = x;
                        //if(alpideX == 1024) {
                        //    for(unsigned int i=0;i<lengthAlpideData;i++){
                        //        printf("%02x ", alpide[i]);
                        //    }
                        //    printf(" ##\n");
                        //}
                        alpideY = y;
                        alpideID = chipid;
                        alpideBX = bxcounter;
                        dataTree->Fill();
                        //alpideMaps.at(chipid)->Fill(alpideX, alpideY);
#endif
                    }
                }
                //if( ( hitmap & 0x01 ){
                //
                //    //printf(" --> position: (x = %d, y = %d)", x, y);
                //}
                
                dataword = 0;
            }
        }
        else {
            //regionheader = false;
            //header = false;
            //dataword = false;
        }
        //printf("\n");
    }
}

int getAlpideY(uint16_t address){
    return address/2;
}

int getAlpideX(uint8_t region, uint8_t encoder, uint16_t address){
    int x = region*32 + encoder*2;
    if(address%4==1) x++;
    if(address%4==2) x++;
    return x;   
}


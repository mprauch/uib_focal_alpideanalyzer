#include "readALPIDETree.h"
#include "writeNoisyPixelBitmap.h"
#include "writeNoisyPixelBitmap.C"
#include <iostream>
#include <fstream>
#include <vector>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <ostream>
#include "TTree.h"
#include "TFile.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <malloc.h>
#include <vector>
#include <cstring>

#include "readALPIDETree.h"
#include "writeNoisyPixelBitmap.h"

using namespace std;

std::vector<noisyPixel> noisyPixels;
std::vector< std::vector<noisyPixel> > noisyPixelsById;

int create_mask_pattern(string filename){
    std::vector<noisyPixel> noisyPixels;
    std::vector< std::vector<noisyPixel> > noisyPixelsById;
    
    TFile *file = new TFile(filename.c_str(), "READ");
    TTree *tree = (TTree*) file->Get("DATA");

    int x, y, id, stave = 0;
    uid_t triggerBX = 0;
    long int nHits;


    // Creating a Canvas
    TCanvas *c0 = new TCanvas( "c0", "c0", 2048, 1024);
    //Specify number of ALPIDEs on a stave
    auto hitmap = new TH2D("","", 1024, -0.5, 1023.5, 512, -0.5, 511.5);


    tree->SetBranchAddress("X", &x);
    tree->SetBranchAddress("Y", &y);
    tree->SetBranchAddress("ID", &id);
    tree->SetBranchAddress("triggerBX", &triggerBX);
    //add stave leaf to tree structure based on RU id
    //tree->SetBranchAddress("STAVE", &stave);


    Long64_t NEntries = tree->GetEntries();
    std::cout<<"Filling hitmap"<<std::endl;
    for(Long64_t i=0;i<TMath::Min(NEntries, (Long64_t) 1000000);i++){
        tree->GetEntry(i);
        hitmap->Fill(x,y);
        
    }
    std::cout<<"Looking for noisy pixels"<<std::endl;
    for(int i=0; i< 1024; i++){
        for(int j=0; j<512; j++){
            if(hitmap->GetBinContent(i,j) > 0){
                noisyPixel np ;
                np.n=hitmap->GetBinContent(i,j);
                np.x=i;
                np.y=j;
                np.id=1;
                noisyPixels.push_back(np);
            }
        }
    }
    std::cout<<"Creating bitmap"<<std::endl;

    std::sort(noisyPixels.begin(), noisyPixels.end(), by_hits());

    std::sort(noisyPixels.begin(), noisyPixels.end(), by_id());
    int lastID = -1;
    for(unsigned int i=0;i<noisyPixels.size();i++){
        if(lastID != noisyPixels.at(i).id){
            std:vector<noisyPixel> np;
            for(int j=0;j<noisyPixels.at(i).id-lastID;j++){
                    noisyPixelsById.push_back(np);
            }
            lastID = noisyPixels.at(i).id;
        }
        noisyPixelsById.at(lastID).push_back(noisyPixels.at(i));
    }


    for(unsigned int i=0;i<noisyPixelsById.size();i++){
        writeNoisyPixelBitmap(i, noisyPixelsById.at(i));
    }
    std::cout<<"Done"<<std::endl;
    hitmap->Draw("colz");
    return 0;
}





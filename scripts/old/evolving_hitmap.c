#include "readALPIDETree.h"
#include "writeNoisyPixelBitmap.h"
#include "writeNoisyPixelBitmap.C"
#include <iostream>
#include <fstream>
#include <vector>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <ostream>
#include "TTree.h"
#include "TFile.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <malloc.h>
#include <vector>
#include <cstring>

#include "readALPIDETree.h"
#include "writeNoisyPixelBitmap.h"

using namespace std;

void set_plot_style()
{
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;

    Double_t stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
    Double_t red[NRGBs] = {0.00, 0.00, 0.87, 1.00, 0.51};
    Double_t green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
    Double_t blue[NRGBs] = {0.51, 1.00, 0.12, 0.00, 0.00};
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

void evolving_hitmap(string filename){
    
    set_plot_style();
    gStyle->SetOptStat(0);

    TFile *file = new TFile(filename.c_str(), "READ");
    TTree *tree = (TTree*) file->Get("DATA");

    int x, y, id, input = 0;
    UInt_t feeIDGBTLink= 0;
    ULong64_t triggerTime = 0;
    long int nHits;
    

    struct datapoint{
        ULong64_t frame_id;
        int col;
        int row;
        int chip_id;
        UInt_t stave;
    };

    std::vector<datapoint> test;

    // Creating a Canvas
    TCanvas *c0 = new TCanvas( "c0", "c0", 1500, 1024);
    auto hitmap = new TH2D("Hitmap all chips", "Hitmap all chips", 30, -0.5, 3071.5, 30, -0.5, 3071.5);
    hitmap->GetXaxis()->SetTitle("Column");
    hitmap->GetYaxis()->SetTitle("Row");

    tree->SetBranchAddress("X", &x);
    tree->SetBranchAddress("Y", &y);
    tree->SetBranchAddress("ID", &id);
    tree->SetBranchAddress("triggerTime", &triggerTime);
    tree->SetBranchAddress("feeIDGBTLink", &feeIDGBTLink);


    Long64_t NEntries = tree->GetEntries();
    for(Long64_t i=0;i<TMath::Min(NEntries, (Long64_t) 1000000);i++){
        tree->GetEntry(i);
        datapoint dp;
        dp.frame_id = triggerTime;
        dp.col = x;
        dp.row = y;
        dp.chip_id = id;
        dp.stave = feeIDGBTLink;
        test.push_back(dp);
    }

    std::sort(test.begin(), test.end(), [](const auto& i, const auto& j) { return i.frame_id < j.frame_id; } );

    // 2 (bot) (0,1023) ( 2560 , 3072) 1 (bot) (1024, 2048) (2560, 3072) 0 (bot) (2048,3072), (2560, 3072)
    // 2 (top) (0,1023) ( 2048 , 2560) 1 (top) (1024, 2048) (2048, 2560) 0 (top) (2048,3072), (2048, 2560)
    // 5 (bot) (0, 1023) (1536 , 2048) 4 (bot) (1024, 2048) (1536, 2048) 3 (bot) (2048,3072), (1536, 2048)
    // 5 (top) (0, 1023) (1024 , 1536) 4 (top) (1024, 2048) (1024, 1536) 3 (top) (2048,3072), (1024, 1536)
    // 8 (bot) (0, 1023) (512  , 1024) 7 (bot) (1024, 2048) (512 , 1024) 6 (bot) (2048,3072), (512,  1024)
    // 8 (top) (0, 1023) (0    , 512)  7 (top) (1024, 2048) (0   , 512 ) 6 (top) (2048,3072), (0    , 512)

    //feeid 3 = bottom (øverst)
    //feeid 2 = top (øverst)
    //feeid 1 = bottom (nederst)
    //feeid 0 = top (nederst)

    //bot must be inverted
    int current_event=0;
    for(int i = 0; i < test.size(); i++){
        if(test[i].frame_id == test[i-1].frame_id){
            if (test[i].chip_id==2 && (test[i].stave==1 || test[i].stave==3)){
                hitmap->Fill(1024 - test[i].col, 3072 - test[i].row);
            }
            else if(test[i].chip_id==1 && (test[i].stave==1 || test[i].stave==3)){
                hitmap->Fill(1024 + (1024-test[i].col), 3072 - test[i].row);
            }
            else if (test[i].chip_id==0 && (test[i].stave==1 || test[i].stave==3)){
                hitmap->Fill(2048 + (1024 - test[i].col), 3072 - test[i].row);
            }
            else if (test[i].chip_id==5 && (test[i].stave==1 || test[i].stave==3)){
                hitmap->Fill(1024 - test[i].col, 2048 - test[i].row);
            }
            else if (test[i].chip_id==4 && (test[i].stave==1 || test[i].stave==3)){
                hitmap->Fill(1024 + (1024-test[i].col), 2048 - test[i].row);
            }
            else if (test[i].chip_id==3 && (test[i].stave==1 || test[i].stave==3)){
                hitmap->Fill(2048 + (1024 - test[i].col), 2048 - test[i].row);
            }
            else if (test[i].chip_id==8 && (test[i].stave==1 || test[i].stave==3)){
                hitmap->Fill(1024 - test[i].col, 1024 - test[i].row);
            }
            else if (test[i].chip_id==7 && (test[i].stave==1 || test[i].stave==3)){
                hitmap->Fill(1024 + (1024-test[i].col), 1024 - test[i].row);
            }
            else if (test[i].chip_id==6 && (test[i].stave==1 || test[i].stave==3)){
                hitmap->Fill(2048 + (1024 - test[i].col), 1024 - test[i].row);
            }

            else if (test[i].chip_id==2 && (test[i].stave==0 || test[i].stave==2)){
                hitmap->Fill(test[i].col, test[i].row+2048);
            }
            else if (test[i].chip_id==1 && (test[i].stave==0 || test[i].stave==2)){
                hitmap->Fill(1024+test[i].col, test[i].row+2048);
            }
            else if (test[i].chip_id==0 && (test[i].stave==0 || test[i].stave==2)){
                hitmap->Fill(2048+test[i].col, test[i].row+2048);
            }
            else if (test[i].chip_id==5 && (test[i].stave==0 || test[i].stave==2)){
                hitmap->Fill(test[i].col, test[i].row+1024);
            }
            else if (test[i].chip_id==4 && (test[i].stave==0 || test[i].stave==2)){
                hitmap->Fill(1024+test[i].col, test[i].row+1024);
            }
            else if (test[i].chip_id==3 && (test[i].stave==0 || test[i].stave==2)){
                hitmap->Fill(2048+test[i].col, test[i].row+1024);
            }
            else if (test[i].chip_id==8 && (test[i].stave==0 || test[i].stave==2)){
                hitmap->Fill(test[i].col, test[i].row);
            }
            else if (test[i].chip_id==7 && (test[i].stave==0 || test[i].stave==2)){
                hitmap->Fill(1024+test[i].col, test[i].row);
            }
            else if (test[i].chip_id==6 && (test[i].stave==0 || test[i].stave==2)){
                hitmap->Fill(2048+test[i].col, test[i].row);
            }
        }
        else{      
                hitmap->Draw("colz");
                c0->Update();
                hitmap->Reset();

                std::cout<<"Current event ID: "<<current_event<<" with timestamp: "<<test[i].frame_id<<"\nPress 0 to stop \nPress 1 to draw hitmap for next event"<<std::endl;
                current_event++;
                cin >>input;
                if(input==0){
                break;
            }   
        }
    }
}
